.PHONY: pip-install lint test serve build release-image deploy

SERVER = 35.234.31.246
HOST_KEY = 35.234.31.246 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDrbU7YQ93TANzyuvzlXA2A1lFYNIondDJITFlsJX88tHA0HIkXHjEXB72pc0y8TmXtB/iEOWouzPOuPlYBTdX0=

check_defined = \
	$(strip $(foreach 1,$1, \
		$(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
	$(if $(value $1),, \
	  $(error Undefined $1$(if $2, ($2))))


pip-install:
	pip3 install -r requirements.txt

lint:
	flake8 --extend-ignore=W605,E722,E501,F401,F403 httpbin

test:
	python3 test_httpbin.py

serve:
	gunicorn -b 0.0.0.0:5000 -k gevent httpbin:app

build:
	$(call check_defined, IMAGE_NAME, IMAGE_NAME is not defined)
	docker build -t $(IMAGE_NAME) .
	docker push $(IMAGE_NAME)

release-image:
	$(call check_defined, IMAGE_NAME, IMAGE_NAME is not defined)
	$(call check_defined, RELEASE_IMAGE_NAME, RELEASE_IMAGE_NAME is not defined)
	docker pull $(IMAGE_NAME)
	docker tag $(IMAGE_NAME) $(RELEASE_IMAGE_NAME)
	docker push $(RELEASE_IMAGE_NAME)

deploy:
	$(call check_defined, SSH_KEY, SSH_KEY is not defined)
	$(call check_defined, IMAGE_NAME, IMAGE_NAME is not defined)
	$(call check_defined, IMAGE_ACCESS_TOKEN, IMAGE_ACCESS_TOKEN is not defined)
	mkdir -p $(HOME)/.ssh
	chmod 0600 $(SSH_KEY)
	echo $(HOST_KEY) >> $(HOME)/.ssh/known_hosts
	ssh -i $(SSH_KEY) digiman@$(SERVER) " \
		docker login -u holyeric -p $(IMAGE_ACCESS_TOKEN) registry.gitlab.com; \
		docker pull $(IMAGE_NAME); \
		(docker stop service && docker rm service) || true; \
		docker run --name service -p 5000:5000 -d $(IMAGE_NAME); "
